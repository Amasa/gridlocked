using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Gridlocked.MonoGame.Core;
using Microsoft.Xna.Framework;

namespace Gridlocked.Android
{
    [Activity(
        Label = "@string/app_name",
        MainLauncher = true,
        Icon = "@drawable/icon",
        AlwaysRetainTaskState = true,
        LaunchMode = LaunchMode.SingleInstance,
        ScreenOrientation = ScreenOrientation.Portrait,
        ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenLayout
    )]
    public class MainActivity : AndroidGameActivity
    {
        private GridlockedGame _game;
        private View _view;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            _game = new GridlockedGame
            {
                StartActivity = StartActivity
            };
            _view = _game.Services.GetService(typeof(View)) as View;

            // https://developer.android.com/training/system-ui/immersive
            //_view.SystemUiVisibility = (StatusBarVisibility)(SystemUiFlags.HideNavigation | SystemUiFlags.Immersive | SystemUiFlags.Fullscreen);

            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

            SetContentView(_view);
            _game.Run();
        }
    }
}
