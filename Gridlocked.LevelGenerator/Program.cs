﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Gridlocked.LevelGenerator
{
    internal static class Program
    {
        private static void Main()
        {
            const int max_number_of_levels = 500;

            var fiveByFiveLevels = GenerateLevels(5, max_number_of_levels);
            var sixBySixLevels = GenerateLevels(6, max_number_of_levels);
            var sevenBySevenLevels = GenerateLevels(7, max_number_of_levels);
            var eightByEightLevels = GenerateLevels(8, max_number_of_levels);
            var nineByNineLevels = GenerateLevels(9, max_number_of_levels);

            var singleSolutionFiveByFiveLevels = ReplaceLevelsWithMultipleSolutions(fiveByFiveLevels);
            var singleSolutionSixBySixLevels = ReplaceLevelsWithMultipleSolutions(sixBySixLevels);
            var singleSolutionSevenBySevenLevels = ReplaceLevelsWithMultipleSolutions(sevenBySevenLevels);
            var singleSolutionEightByEightLevels = ReplaceLevelsWithMultipleSolutions(eightByEightLevels);
            var singleSolutionNineByNineLevels = ReplaceLevelsWithMultipleSolutions(nineByNineLevels);
            
            WriteLevelListToFile(singleSolutionFiveByFiveLevels);
            WriteLevelListToFile(singleSolutionSixBySixLevels);
            WriteLevelListToFile(singleSolutionSevenBySevenLevels);
            WriteLevelListToFile(singleSolutionEightByEightLevels);
            WriteLevelListToFile(singleSolutionNineByNineLevels);

            Console.ReadKey();
        }

        private static IReadOnlyList<string> ReplaceLevelsWithMultipleSolutions(IReadOnlyList<string> levelCodes)
        {
            string[] levelsWithUniqueSolutions = levelCodes.ToArray();
            int size = GetSizeFromLevelCode(levelCodes[0]);
            
            var randomizer = new Random(0);
            var index = 0;
            var numLevelsReplaced = 0;
            while (index < levelsWithUniqueSolutions.Length)
            {
                string levelCode = levelsWithUniqueSolutions[index];

                bool hasSingleSolution = HasSingleSolution(levelCode);

                if (!hasSingleSolution)
                {
                    while (true)
                    {
                        var newLevel = Generate.Grid(size, randomizer, out string errorMessage);

                        if (errorMessage != null)
                        {
                            continue;
                        }

                        string newLevelCode = newLevel.Encode();

                        if (levelsWithUniqueSolutions.Contains(newLevelCode)
                            || !HasSingleSolution(newLevelCode))
                        {
                            continue;
                        }

                        levelsWithUniqueSolutions[index] = newLevelCode;
                        numLevelsReplaced += 1;
                        break;
                    }
                }

                index += 1;
            }

            Console.WriteLine($"Replaced {numLevelsReplaced} {size}x{size} levels that had multiple solutions.");
            
            return levelsWithUniqueSolutions;
        }

        private static IReadOnlyList<string> GenerateLevels(int size, int levelsToGenerate)
        {
            var startedTime = DateTime.Now;

            Console.WriteLine($"Generating {size}x{size} levels... started at {startedTime.ToShortTimeString()}");

            var bad = 0;
            var good = 0;

            // GENERATE LEVELS
            var levels = new HashSet<string>();
            var randomizer = new Random(0);
            while (levels.Count < levelsToGenerate)
            {
                var candidate = Generate.Grid(size, randomizer, out string errorMessage);

                if (errorMessage == null)
                {
                    string levelCode = candidate.Encode();

                    levels.Add(levelCode);
                    good += 1;
                }
                else
                {
                    //Console.WriteLine($"Error generating grid: {errorMessage}");
                    bad += 1;
                }

                //Console.WriteLine($"Generating levels: BAD: {bad}, GOOD: {good}");
            }

            Console.WriteLine($"Generated levels: BAD: {bad}, GOOD: {good}");

            // ORDER ALL LEVELS BY "DIFFICULTY" (WIP?)

            var allDirections = Get.Enums<BlockDirection>();

            var totalInternalTCrosses = new Dictionary<int, int>();
            var totalNumSteps = new Dictionary<int, int>();
            void AddTotalNumSteps(int numSteps)
            {
                if (!totalNumSteps.ContainsKey(numSteps))
                {
                    totalNumSteps[numSteps] = 0;
                }

                totalNumSteps[numSteps] += 1;
            }

            var levelNumber = 0;
            string[] levelsFromEasiestToHardest = levels.ToArray()
                .Select(levelCode => levelCode.Decode())
                .OrderBy(level =>
                {
                    // SET "STEPS"

                    levelNumber += 1;
                    level.Reset(new Random(levelNumber));

                    if (!level.IsSolvable(out int steps))
                    {
                        //Console.WriteLine($"Level {levelNumber}: Not solvable (?!)");

                        AddTotalNumSteps(int.MaxValue);

                        return int.MaxValue;
                    }

                    //Console.WriteLine($"Level {levelNumber}: {steps} steps to solve");
                    AddTotalNumSteps(steps);

                    // SET T-CROSSES

                    var numInternalTCrosses = 0;

                    foreach (var block in level.Blocks)
                    {
                        if (block.Type == BlockType.TCross
                            && allDirections.All(direction => block.BlockTo(direction) != null))
                        {
                            numInternalTCrosses += 1;
                        }
                    }

                    //Console.WriteLine($"Found {numInternalTs} internal T-crosses.");

                    if (!totalInternalTCrosses.ContainsKey(numInternalTCrosses))
                    {
                        totalInternalTCrosses[numInternalTCrosses] = 0;
                    }
                    totalInternalTCrosses[numInternalTCrosses] += 1;

                    return steps + (numInternalTCrosses * 2); // Multiply by 1-4
                })
                .Select(level => level.Encode())
                .ToArray();

            // TAKE THE 10 LEAST DIFFICULT LEVELS (SHUFFLED),
            // FOLLOWED BY THE REST OF THE LEVELS (SHUFFLED).

            string[] allLevels = levelsFromEasiestToHardest
                .Take(10)
                .OrderBy(l => randomizer.Next())
                .Concat(
                    levelsFromEasiestToHardest
                        .Skip(10)
                        .OrderBy(l => randomizer.Next())
                )
                .ToArray();

            var endedTime = DateTime.Now;
            Console.WriteLine($"DONE. Generated {levels.Count} {size}x{size} levels.");
            Console.WriteLine($"Started at {startedTime.ToShortTimeString()}, ended at {endedTime.ToShortTimeString()}.");
            Console.WriteLine($"Took {(endedTime - startedTime)} to complete.");
            Console.WriteLine("-----------------");

            return allLevels.ToArray();
            // Console.ReadKey();
        }

        private static void WriteLevelListToFile(IReadOnlyList<string> levels)
        {
            int size = GetSizeFromLevelCode(levels[0]);
            string allLevelsFileText = "- " + string.Join("\n- ", levels);
            File.WriteAllText($"{size}x{size}Levels.yaml", allLevelsFileText);
            Console.WriteLine($"Wrote {size}x{size} levels to yaml file.");
        }

        private static int GetSizeFromLevelCode(string levelCode)
        {
            return (int) Math.Sqrt(levelCode.Length);
        }

        private static bool HasSingleSolution(string levelCode)
        {
            string[][] patternsIndicatingMultipleSolutions =
            {
                new [] { "03", "30" },
                new [] { "30", "03" },
                new [] { "303", "020" },
                new [] { "020", "303" },
                new [] { "03", "20", "03" },
                new [] { "30", "02", "30" },
                // new [] { "0223", "3023" },
                // new [] { "3023", "0223" },
                // new [] { "3220", "3203" },
                // new [] { "3203", "3220" },
                // new [] { "30", "02", "22", "33" },
                // new [] { "33", "22", "02", "30" },
                // new [] { "03", "20", "22", "33" },
                // new [] { "33", "22", "20", "03" }
            };

            int numPatternsMatching = patternsIndicatingMultipleSolutions
                .Count(pattern => PatternMatchesLevel(pattern, levelCode));

            return numPatternsMatching == 0;
        }

        private static bool PatternMatchesLevel(IReadOnlyList<string> pattern, string levelCode)
        {
            int size = GetSizeFromLevelCode(levelCode);
            string standardizedLevelCode = levelCode
                .Replace('H', '0')
                .Replace('M', '0')
                .Replace('N', '1')
                .Replace('O', '2')
                .Replace('P', '3');

            if (!standardizedLevelCode.All(char.IsDigit))
            {
                throw new ApplicationException($"Level code standardization failed: {levelCode} turned into {standardizedLevelCode}.");
            }
            
            var index = 0;
            while (index < standardizedLevelCode.Length - pattern.Count * size - pattern[0].Length)
            {
                int columnNumber = index % size;
                if (columnNumber + pattern[0].Length > size)
                {
                    index += 1;
                    continue;
                }
                
                var patternMatches = true;
                var patternPartIndex = 0;
                foreach (string patternPart in pattern)
                {
                    string correspondingLevelPart = standardizedLevelCode.Substring(index + patternPartIndex * size, patternPart.Length);

                    if (patternPart != correspondingLevelPart)
                    {
                        patternMatches = false;
                        break;
                    }
                    
                    patternPartIndex += 1;
                }

                if (patternMatches)
                {
                    return true;
                }
                
                index += 1;
            }
            
            return false;
        }
    }
}
