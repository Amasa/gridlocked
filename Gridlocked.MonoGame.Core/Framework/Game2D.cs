﻿// Author: Alex Leone

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Screens;
using Gridlocked.MonoGame.Core.Helpers;
using MonoGame.Extended.Screens.Transitions;
#if ANDROID
using System;
using Android.Content;
#endif

namespace Gridlocked.MonoGame.Core
{
    public class Game2D
        : Game
    {
        private readonly ScreenManager screenManager = new ScreenManager();
        private int FramesToDrawUntilClean { get; set; } = 0;

        protected GraphicsDeviceManager Graphics { get; }

        internal SpriteBatch SpriteBatch { get; private set; }

        internal virtual Rectangle Bounds => new Rectangle(0, 0, 300, 500);

        internal Point InputOffset => GraphicsDevice.Viewport.Bounds.Location;
        internal void FlagDirty()
        {
            FramesToDrawUntilClean = 10;
        }

        protected Game2D()
        {
            Graphics = new GraphicsDeviceManager(this)
            {
                SupportedOrientations = DisplayOrientation.Portrait | DisplayOrientation.PortraitDown
            };

            Content.RootDirectory = "Content";
            IsMouseVisible = true;

            Components.Add(screenManager);

            this.Activated += Game2D_Activated;
        }

        private void Game2D_Activated(object sender, System.EventArgs e)
        {
            this.FlagDirty();
        }

        internal bool LockInput { get; private set; }

#if ANDROID
        public Action<Intent> StartActivity { get; internal set; }
#endif

        internal void LoadScreen(GameScreen screen, bool fade = false)
        {
            FlagDirty();

            if (fade)
            {
                Transition transition = new BetterFadeTransition(GraphicsDevice, Colors.Background, duration: 10);

                LockInput = true;
                transition.Completed += (o, e) => LockInput = false;
                
                screenManager.LoadScreen(screen, transition);
            }
            else
            {
                screenManager.LoadScreen(screen);
            }
        }

        protected override void Initialize()
        {
            Resolution.Init(Graphics);
            Resolution.SetVirtualResolution(Bounds.Width, Bounds.Height);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Draw(GameTime gameTime)
        {
            if (FramesToDrawUntilClean == 0)
            {
                return;
            }

            Resolution.BeginDraw();

            SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, transformMatrix: Resolution.GetTransformationMatrix());

            base.Draw(gameTime);

            SpriteBatch.End();

            FramesToDrawUntilClean -= 1;
        }
    }
}
