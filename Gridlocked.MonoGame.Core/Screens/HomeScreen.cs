﻿// Author: Alex Leone

#if ANDROID
using Android.Content;
using Android.Net;
#endif
using Gridlocked.Core.Enums;
using Gridlocked.Core.Resources;
using Gridlocked.MonoGame.Core.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGame.Extended.Input;
using MonoGame.Extended.Screens;
using System.Linq;
#if !ANDROID
using System.Diagnostics;
#endif

namespace Gridlocked.MonoGame.Core.Screens
{
    public class HomeScreen : GameScreen
    {
        private const string share_url = "https://leonegaming.itch.io/gridlocked";
#if GOOGLEPLAY
        private const string CONTACT_URL = "https://alexjleone.com/contact?subject=Gridlocked#message";
#else
        private const string donate_url = "https://alexjleone.com/projects/donate?p=gridlocked#donate";
#endif

        private readonly Difficulty difficulty;
        private SourceType sourceType = SourceType.PowerStation;

        private readonly ClickableButton playButton;
        private readonly ClickableButton shareButton;
        private readonly ClickableButton donateContactButton;

        private readonly ClickableButton powerButton;
        private readonly ClickableButton waterButton;

        private new GridlockedGame Game => (GridlockedGame)base.Game;

        private const int top_edge_of_title = 55;

        internal HomeScreen(GridlockedGame game, SourceType sourceType, Difficulty difficulty) : base(game)
        {
            this.sourceType = sourceType;
            this.difficulty = difficulty;

            const int margin = 20;
            const int height = 50;
            const int top_edge_of_buttons = 170;

            var playButtonRect = new Rectangle(margin, top_edge_of_buttons, Game.Bounds.Width - margin * 2, height);
            playButton = new ClickableButton
            {
                Rect = playButtonRect,
                Data = Strings.Play
            };
            shareButton = new ClickableButton
            {
                Rect = new Rectangle(playButtonRect.Location + new Point(0, margin + height), playButtonRect.Size),
                Data = Strings.Share
            };
            donateContactButton = new ClickableButton
            {
                Rect = new Rectangle(playButtonRect.Location + new Point(0, (margin + height) * 2), playButtonRect.Size),
                Data =
#if GOOGLEPLAY
                    Strings.Contact
#else
                    Strings.Donate
#endif
            };

            powerButton = new ClickableButton
            {
                Rect = new Rectangle(50, 380, 100, 100),
                Data = SourceType.PowerStation
            };
            waterButton = new ClickableButton
            {
                Rect = new Rectangle(150, 380, 100, 100),
                Data = SourceType.WaterTower
            };
        }

        private void Share(string url)
        {
#if ANDROID
            Intent shareIntent = new Intent();
            shareIntent.SetAction(Intent.ActionSend);
            shareIntent.PutExtra(Intent.ExtraText, $"Check out this game, Gridlocked! {url}");
            shareIntent.SetType("text/plain");
            Intent.CreateChooser(shareIntent, "Share via");
            Game.StartActivity(shareIntent);
            Game.FlagDirty();
#else
            OpenLink(url);
#endif
        }

        private void OpenLink(string url)
        {
#if ANDROID
            var uri = Uri.Parse(url);
            var openLinkIntent = new Intent(Intent.ActionView, uri);
            Game.StartActivity(openLinkIntent);
            Game.FlagDirty();
#else
            Process.Start(new ProcessStartInfo(url) { UseShellExecute = true });
#endif
        }

        public override void Update(GameTime gameTime)
        {
            var mouseState = MouseExtended.GetState();
            var touches = TouchPanel.GetState();
            object[] clickResults = mouseState.ProcessButtonClicks(
                touches,
                Resolution.GetTransformationMatrix(),
                offset: Game.InputOffset,
                new[] { playButton, shareButton, donateContactButton, powerButton, waterButton },
                gameTime,
                Game.FlagDirty
            );

            if (clickResults.Contains(playButton.Data))
            {
                Game.LoadScreen(new SizeSelectScreen(Game, sourceType, difficulty));
            }
            else if (clickResults.Contains(shareButton.Data))
            {
                Share(share_url);
            }
            else if (clickResults.Contains(donateContactButton.Data))
            {
#if GOOGLEPLAY
                OpenLink(CONTACT_URL);
#else
                OpenLink(donate_url);
#endif
            }

            else if (clickResults.Contains(powerButton.Data))
            {
                sourceType = SourceType.PowerStation;
                Save.Progress(Game.Levels, sourceType, difficulty, Game.HasViewedTutorial, gridProgress: null);
            }
            else if (clickResults.Contains(waterButton.Data))
            {
                sourceType = SourceType.WaterTower;
                Save.Progress(Game.Levels, sourceType, difficulty, Game.HasViewedTutorial, gridProgress: null);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Colors.Background);

            // DRAW TITLE

            bool isPowerStation = sourceType == SourceType.PowerStation;
            var titleColor = isPowerStation
                ? Colors.Powered
                : Colors.Flowing;

            Game.SpriteBatch.DrawCenteredText(
                "Grid",
                Game.Fonts.TitleText,
                new Vector2(Game.Bounds.Center.X, top_edge_of_title),
                titleColor
            );

            Game.SpriteBatch.DrawCenteredText(
                "locked",
                Game.Fonts.TitleText,
                new Vector2(Game.Bounds.Center.X, top_edge_of_title + 55),
                titleColor
            );

            // DRAW MENU

            var menuFont = Game.Fonts.RegularText;
            Game.SpriteBatch.DrawButton(playButton, menuFont);
            Game.SpriteBatch.DrawButton(shareButton, menuFont);
            Game.SpriteBatch.DrawButton(donateContactButton, menuFont);

            // DRAW WATER / POWER SELECTOR

            Game.SpriteBatch.Draw(
                Game.Textures.LightningBolt,
                powerButton.Rect,
                isPowerStation
                    ? Colors.Powered
                    : (
                        powerButton.Highlighted
                            ? Colors.Lock
                            : Colors.UnConnected
                      )
            );

            Game.SpriteBatch.Draw(
                Game.Textures.WaterDrop,
                waterButton.Rect,
                !isPowerStation
                    ? Colors.Flowing
                    : (
                        waterButton.Highlighted
                            ? Colors.Lock
                            : Colors.UnConnected
                      )
            );
        }
    }
}
