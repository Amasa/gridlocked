﻿// Author: Alex Leone

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Gridlocked.Core.Enums;
using Gridlocked.Core.Logic;
using Gridlocked.Core.Models;
using Gridlocked.Core.Resources;
using Gridlocked.MonoGame.Core.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGame.Extended;
using MonoGame.Extended.Input;
using MonoGame.Extended.Screens;

namespace Gridlocked.MonoGame.Core.Screens
{
    public class GridScreen : GameScreen
    {
        private new GridlockedGame Game => (GridlockedGame)base.Game;

        private readonly Grid grid;
        private readonly SourceType sourceType;

#if DEBUG
        private IEnumerator<string> solution;
        private const float solution_delay = 0.2F;
        private double lastSolveStepTime;
#endif

        private readonly byte size;
        private readonly int level;

        private readonly TopBar topBar;
        private readonly ClickableButton[] blockButtons;
        private readonly ClickableButton nextLevelButton;
        private readonly ClickableButton resetButton;
        
        private readonly ClickableButton tutorialButton;

        private readonly ClickableButton tutorialModal;
        private readonly string tutorialMessage;
        private readonly ClickableButton okayButton;

        private bool tutorialVisible;

        internal GridScreen(GridlockedGame game, SourceType sourceType, byte size, Difficulty difficulty, int level, GridProgress gridProgress) : base(game)
        {
            this.sourceType = sourceType;
            this.size = size;
            this.level = level;

            grid = Game.Levels[size].EncodedLevels[level - 1].Decode();
            grid.Difficulty = difficulty;

            if (gridProgress == null)   
            {
                grid.Reset(new Random(level)); // Shuffling, but always the same for each level.
                Save.Progress(Game.Levels, sourceType, difficulty, Game.HasViewedTutorial, grid.GetProgress(level));
            }
            else
            {
                grid.Set(gridProgress);
            }


            topBar = new TopBar(
                $"{level} / {game.Levels[size].MaxLevelsUnlocked[difficulty]}",
                Game.Bounds.Width
            );

            tutorialButton = new ClickableButton
            {
                Rect = new RectangleF(
                    new Point2(
                        Game.Bounds.Width - topBar.BackButton.Rect.X - topBar.BackButton.Rect.Width + 3,
                        topBar.BackButton.Rect.Y + 3
                    ),
                    new Size2(
                        topBar.BackButton.Rect.Size.Width - 6,
                        topBar.BackButton.Rect.Size.Height - 6
                    )
                ),
                Data = "T"
            };
            string action =
#if ANDROID
                    Strings.Tap;
#else
                    Strings.Click;
#endif
            string source = IsPowerStation
                ? Strings.PowerStation
                : Strings.WaterTower;
            string connector = IsPowerStation
                ? Strings.Wires
                : Strings.Pipes;
            this.tutorialMessage = string.Format(Strings.TutorialText, action, source, connector);
            int numLines = tutorialMessage.Count(c => c == '\n') + 1;

            var modalBounds = Game.Bounds;
            modalBounds.Inflate(-30, -55);

            if (numLines > 9)
            {
                modalBounds.Inflate(0, Game.Fonts.RegularText.LineHeight / 2);
            }

            tutorialModal = new ClickableButton
            {
                Rect = modalBounds,
                Data = "Modal"
            };
            okayButton = new ClickableButton
            {
                Rect = new Rectangle(
                    modalBounds.Center.X - 48,
                    modalBounds.Bottom - 10 * 3 - 48,
                    48 * 2,
                    48
                ),
                Data = Strings.Okay
            };

            var blockButtonsList = new List<ClickableButton>();
            ForEachBlock.Do(size, (x, y) =>
            {
                var position = GridOrigin + new Vector2(BlockSize * x, BlockSize * y);

                blockButtonsList.Add(new ClickableButton
                {
                    Rect = new RectangleF(
                        position.X,
                        position.Y,
                        BlockSize,
                        BlockSize
                    ),
                    Data = (x, y)
                });
            });
            blockButtons = blockButtonsList.ToArray();

            {
                const int height = 50;
                const int margin = 20;
                const int min_width = 150;
                /*const*/ int maxWidth = Game.Bounds.Width - margin * 2; // Can't be const, because Rectangles can't be const.

                var nextLevelTextSize = Game.Fonts.RegularText.MeasureString(Strings.NextLevel);
                int width = (int)nextLevelTextSize.X + margin * 2;
                width = Math.Clamp(width, min_width, maxWidth);

                var nextLevelRect = new Rectangle(
                    (GridOrigin + new Vector2((GridWidthOrHeight - width) / 2, GridWidthOrHeight + margin)).ToPoint(),
                    new Point(width, height)
                );
                nextLevelButton = new ClickableButton
                {
                    Rect = nextLevelRect,
                    Data = LastLevelInBlock
                        ? Strings.Return
                        : Strings.NextLevel
                };

                resetButton = new ClickableButton
                {
                    Rect = nextLevelRect,
                    Data = "Reset"
                };
            }

            if (!Game.HasViewedTutorial)
            {
                tutorialVisible = true;
            }
        }

        public override void Update(GameTime gameTime)
        {
            var mouseState = MouseExtended.GetState();
            var touches = TouchPanel.GetState();
            var matrix = Resolution.GetTransformationMatrix();

            if (tutorialVisible)
            {
                var modalClickResults = mouseState.ProcessButtonClicks(
                    touches,
                    matrix,
                    offset: Game.InputOffset,
                    new[] { okayButton, tutorialModal },
                    gameTime,
                    Game.FlagDirty
                );

                if (modalClickResults.Contains(okayButton.Data)
                    || (modalClickResults.Any() && !modalClickResults.Contains(tutorialModal.Data)))
                {
                    tutorialVisible = false;
                    Game.HasViewedTutorial = true;

                    Save.Progress(Game.Levels, sourceType, grid.Difficulty, hasViewedTutorial: true, grid.GetProgress(level));
                }

                return;
            }

#if DEBUG
            // ADVANCE SOLUTION ATTEMPT
            if (solution != null && lastSolveStepTime + solution_delay < gameTime.TotalGameTime.TotalSeconds)
            {
                if (solution.MoveNext())
                {
                    if (solution.Current != null)
                    {
                        Debug.WriteLine(solution.Current);
                    }

                    lastSolveStepTime = gameTime.TotalGameTime.TotalSeconds;
                }
                else
                {
                    solution = null;
                }
            }
#endif

            // ACCEPT GRID AND BUTTON CLICKS
            object[] clickResults = mouseState.ProcessButtonClicks(
                touches,
                matrix,
                offset: Game.InputOffset,
                blockButtons.Concat(new[] { topBar.BackButton, nextLevelButton, resetButton, tutorialButton }),
                gameTime,
                Game.FlagDirty
            );
            
            if (clickResults.Contains(topBar.BackButton.Data))
            {
                Save.Progress(Game.Levels, sourceType, grid.Difficulty, Game.HasViewedTutorial, gridProgress: null);
                Game.LoadScreen(new LevelSelectScreen(Game, sourceType, size, grid.Difficulty));
            }
            else if (!grid.IsSolved && clickResults.Contains(tutorialButton.Data))
            {
                tutorialVisible = true;
            }
            else if (grid.IsSolved && clickResults.Contains(nextLevelButton.Data))
            {
                if (LastLevelInBlock)
                {
                    Game.LoadScreen(new LevelSelectScreen(Game, sourceType, size, grid.Difficulty));
                }
                else
                {
                    Game.LoadScreen(new GridScreen(Game, sourceType, size, grid.Difficulty, level + 1, gridProgress: null));
                }
            }
            else if (!grid.IsSolved && clickResults.Contains(resetButton.Data))
            {
                grid.Reset(new Random(level));
                Save.Progress(Game.Levels, sourceType, grid.Difficulty, Game.HasViewedTutorial, grid.GetProgress(level));
            }
            else if (!grid.IsSolved && clickResults.Any(r => r != null))
            {
                (byte x, byte y) = ((byte, byte)) clickResults.FirstOrDefault(r => r != null);

                grid.RotateBlock(x, y);

                if (grid.IsSolved)
                {
                    Game.Levels[size].MaxLevelsBeaten[grid.Difficulty] = Math.Max(level, Game.Levels[size].MaxLevelsBeaten[grid.Difficulty]);

                    Save.Progress(Game.Levels, sourceType, grid.Difficulty, Game.HasViewedTutorial, gridProgress: null);
                }
                else
                {
                    Save.Progress(Game.Levels, sourceType, grid.Difficulty, Game.HasViewedTutorial, grid.GetProgress(level));
                }
            }
            else if (clickResults.Any(r => r == null))
            {
                grid.Deselect();
                Save.Progress(Game.Levels, sourceType, grid.Difficulty, Game.HasViewedTutorial, grid.GetProgress(level));
            }
        }

        private bool LastLevelInBlock => level == Game.Levels[size].MaxLevelsUnlocked[grid.Difficulty];

        private Vector2 GridOrigin => GridRect.Location.ToVector2();
        private Rectangle GridRect => new Rectangle(0, (ScreenHeight - GridWidthOrHeight) / 2, ScreenWidth, ScreenWidth);

        private int GridWidthOrHeight => ScreenWidth;
        private int ScreenWidth => Game.Bounds.Width;
        private int ScreenHeight => Game.Bounds.Height;
        private int GridSize => grid.Blocks.GetLength(0);
        private float BlockSize => (float) ScreenWidth / GridSize;
        private bool IsPowerStation => sourceType == SourceType.PowerStation;

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Colors.Background);

            // DRAW GRID OUTLINES
            {
                const int grid_thickness = 3;

                Game.SpriteBatch.DrawRectangle(
                    location: GridRect.Location.ToVector2(),
                    GridRect.Size,
                    Colors.Grid,
                    thickness: grid_thickness,
                    layerDepth: Layers.GRID
                );

                if (!grid.IsSolved)
                {
                    // Vertical lines
                    for (byte x = 1; x < GridSize; x += 1)
                    {
                        Game.SpriteBatch.DrawLine(
                            GridOrigin + new Vector2(BlockSize * x, 0),
                            GridOrigin + new Vector2(BlockSize * x, GridWidthOrHeight),
                            Colors.Grid,
                            thickness: grid_thickness,
                            layerDepth: Layers.GRID
                        );
                    }

                    // Horizontal lines
                    for (byte y = 1; y < GridSize; y += 1)
                    {
                        Game.SpriteBatch.DrawLine(
                            GridOrigin + new Vector2(0, BlockSize * y),
                            GridOrigin + new Vector2(GridWidthOrHeight, BlockSize * y),
                            Colors.Grid,
                            thickness: grid_thickness,
                            layerDepth: Layers.GRID
                        );
                    }
                }
            }

            // DRAW BLOCKS
            {
                static float Rotation(BlockDirection direction)
                {
                    return direction switch
                    {
                        BlockDirection.Up => 0,
                        BlockDirection.Right => MathHelper.PiOver2,
                        BlockDirection.Down => MathHelper.Pi,
                        BlockDirection.Left => MathHelper.Pi + MathHelper.PiOver2,
                        _ => throw new ArgumentOutOfRangeException()
                    };
                }

                foreach (var blockButton in blockButtons)
                {
                    (byte x, byte y) = ((byte, byte)) blockButton.Data;
                    var block = grid.Blocks[x, y];
                    var blockTexture = Game.Textures.Blocks[block.Type];
                    var blockColor = block.IsPowered
                        ? (
                            IsPowerStation
                                ? Colors.Powered
                                : Colors.Flowing
                          )
                        : Colors.UnConnected;
                    float scale = BlockSize / blockTexture.Width;
                    var origin = blockTexture.Bounds.Center.ToVector2();

                    if (block == grid.LastBlockRotated)
                    {
                        Game.SpriteBatch.FillRectangle(
                            GridOrigin + new Vector2(BlockSize * x, BlockSize * y),
                            new Vector2(BlockSize),
                            Colors.Highlighted,
                            layerDepth: Layers.BACKGROUND
                        );
                    }

                    void DrawOnBlock(Texture2D texture, Color color, float layerDepth, float rotation = 0)
                    {
                        Game.SpriteBatch.Draw(
                            texture,
                            position: blockButton.Rect.Center.ToVector2(),
                            sourceRectangle: null,
                            color,
                            rotation: rotation,
                            origin: origin,
                            scale: Vector2.One * scale,
                            SpriteEffects.None,
                            layerDepth: layerDepth
                        );
                    }

                    DrawOnBlock(blockTexture, blockColor, layerDepth: Layers.BLOCK, rotation: Rotation(block.Direction));

                    if (block.IsHouse || block.IsPowerStation)
                    {
                        var overlayTexture = block.IsHouse
                            ? Game.Textures.House
                            : (
                                IsPowerStation
                                    ? Game.Textures.PowerStation
                                    : Game.Textures.WaterTower
                              );

                        DrawOnBlock(overlayTexture, blockColor, layerDepth: Layers.BASE);
                    }

                    if (block.IsLocked && !grid.IsSolved)
                    {
                        DrawOnBlock(
                            Game.Textures.Lock,
                            Colors.Lock,
                            layerDepth: Layers.BACKGROUND);

                        DrawOnBlock(Game.Textures.LockInside, Colors.Background * 0.8F, layerDepth: Layers.OVERLAY);
                    }
                }
            }

            // BACK ARROW

            Game.SpriteBatch.Draw(topBar, Game.Textures, Game.Fonts);

            if (grid.IsSolved)
            {
                // NEXT LEVEL
                Game.SpriteBatch.DrawButton(nextLevelButton, Game.Fonts.RegularText);
            }
            else
            {
                // RESET LEVEL

                Game.SpriteBatch.DrawRectangle(
                    resetButton.Rect,
                    Color.White,
                    thickness: resetButton.Highlighted ? 3 : 2,
                    layerDepth: Layers.BASE
                );

                const int reset_button_size = 40;
                var resetRect = resetButton.Rect.ToRectangle();
                Game.SpriteBatch.Draw(
                    Game.Textures.ResetArrow,
                    new Rectangle(
                        resetRect.Location + new Point(resetRect.Width / 2 - reset_button_size / 2, resetRect.Height / 2 - reset_button_size / 2),
                        new Point(reset_button_size)
                    ),
                    Color.White,
                    layerDepth: Layers.BASE
                );
            }

            // TUTORIAL (?) BUTTON

            if (!grid.IsSolved)
            {
                Game.SpriteBatch.Draw(
                    Game.Textures.TutorialIcon,
                    tutorialButton.Rect,
                    tutorialButton.Highlighted
                        ? Color.White
                        : Colors.ButtonDown,
                    layerDepth: Layers.OVERLAY
                );
            }

            // TUTORIAL

            if (tutorialVisible)
            {
                Game.SpriteBatch.DrawModal(
                    tutorialModal,
                    tutorialMessage,
                    Game.Bounds,
                    Game.Fonts,
                    okayButton
                );
            }
        }
    }
}
