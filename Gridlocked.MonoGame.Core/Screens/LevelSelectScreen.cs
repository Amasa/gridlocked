﻿// Author: Alex Leone

using System.Collections.Generic;
using System.Linq;
using Gridlocked.Core.Enums;
using Gridlocked.Core.Resources;
using Gridlocked.MonoGame.Core.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGame.Extended;
using MonoGame.Extended.Input;
using MonoGame.Extended.Screens;

namespace Gridlocked.MonoGame.Core.Screens
{
    public class LevelSelectScreen : GameScreen
    {
        private readonly SourceType sourceType;
        private readonly byte size = 5;
        private readonly Difficulty difficulty;
        private readonly float maxOffset;
        private const float drag_threshhold = 5;

        private readonly TopBar topBar;
        private readonly ClickableButton[] levelButtons;

        private double dragOffset;
        private Point? dragStart;
        //private double speed = 0;
        //private const float accelerationDrag = 15;

        private const int level_button_width = 48;
        private const int level_button_margin = 10;
        
        private int MaxLevelBeaten => Game.Levels[size].MaxLevelsBeaten[difficulty];
        
        private int NextLevel => MaxLevelBeaten + 1;

        private new GridlockedGame Game => (GridlockedGame)base.Game;

        internal LevelSelectScreen(GridlockedGame game, SourceType sourceType, byte size, Difficulty difficulty) : base(game)
        {
            this.sourceType = sourceType;
            this.size = size;
            this.difficulty = difficulty;

            topBar = new TopBar($"{size}x{size} {Strings.Levels}", Game.Bounds.Width);

            int totalLevels = Game.Levels[size].MaxLevelsUnlocked[difficulty];
            var level = 1;

            var levelButtonsList = new List<ClickableButton>();
            for (var row = 0; row < totalLevels / 5; row += 1)
            {
                for (byte column = 0; column < 5; column += 1)
                {
                    var buttonRect = new Rectangle(
                        level_button_margin + column * (level_button_width + level_button_margin),
                        level_button_margin * 2 + level_button_width + row * (level_button_width + level_button_margin),
                        level_button_width,
                        level_button_width
                    );

                    levelButtonsList.Add(new ClickableButton
                    {
                        Rect = buttonRect,
                        Data = level,
#if DEBUG
                        Disabled = false
#else
                        Disabled = level > NextLevel
#endif
                    });

                    level += 1;
                }
            }
            levelButtons = levelButtonsList.ToArray();

            maxOffset = 500 - (level_button_margin * 2 + level_button_width + (totalLevels / 5) * (level_button_width + level_button_margin));
            int currentLevelRow = Game.Levels[size].MaxLevelsBeaten[difficulty] / 5 - 2;
            dragOffset = -((level_button_margin * 2 + level_button_width + currentLevelRow * (level_button_width + level_button_margin)));
        }

        public override void Update(GameTime gameTime)
        {
            var mouseState = MouseExtended.GetState();
            var touches = TouchPanel.GetState();

            // Click buttons
            var matrix = Resolution.GetTransformationMatrix();
            var dragOffsetPoint = new Point(0, (int)dragOffset).TransformPosition(Matrix.Invert(matrix));
            var offset = Game.InputOffset;

            // BACK CLICK RESULTS MUST BE CALLED FIRST https://stackoverflow.com/questions/15715907/handle-back-button-in-monogame-on-android#16131335
            object[] backClickResults = mouseState.ProcessButtonClicks(touches, matrix, offset, new[] { topBar.BackButton, topBar.Backdrop }, gameTime, Game.FlagDirty);
            object[] clickResults = mouseState.ProcessButtonClicks(touches, matrix, offset + dragOffsetPoint, levelButtons, gameTime, Game.FlagDirty);
            
            if (backClickResults.Contains(topBar.BackButton.Data))
            {
                Game.LoadScreen(new SizeSelectScreen(Game, sourceType, difficulty));
            }
            else if (!backClickResults.Contains(topBar.Backdrop.Data) && clickResults.Any(click => click != null))
            {
                object level = clickResults[0];
                if (level is int)
                {
                    Game.LoadScreen(new GridScreen(Game, sourceType, size, difficulty, (int)level, gridProgress: null));
                }
            }
            
            (
                bool wasInputJustDown,
                bool isInputDown,
                bool _,
                var rawInputPosition,
                var dragDistance
            ) = mouseState.GetInputState(touches, matrix);
            var windowRect = Game.Window.ClientBounds;
            windowRect.Offset(Game.Window.ClientBounds.Location.ToVector2() * -1);
            if (wasInputJustDown)
            {
                dragStart = rawInputPosition;
            }
            else if (
                dragDistance != Point.Zero &&
                windowRect.Contains(rawInputPosition) && // Stop dragging once we get off the edge of the screen.
                (!dragStart.HasValue || Vector2.Distance(rawInputPosition.ToVector2(), dragStart.Value.ToVector2()) >= drag_threshhold)
            )
            {
                dragOffset -= dragDistance.Y;
                //speed += dragDistance.Y * gameTime.ElapsedGameTime.TotalSeconds;

                foreach (var button in levelButtons)
                {
                    button.Highlighted = false;
                }
            }
            else if (!isInputDown)
            {
                dragStart = null;

                //if (Math.Abs(speed) > 0.1F)
                //{
                //    var decelleration = Math.Sign(speed) * (accelerationDrag * gameTime.ElapsedGameTime.TotalSeconds);

                //    if (Math.Abs(decelleration) < Math.Abs(speed))
                //    {
                //        speed -= decelleration;
                //    }
                //    else
                //    {
                //        speed = 0;
                //    }

                //    dragOffset -= speed / gameTime.ElapsedGameTime.TotalSeconds;
                //}
                //else
                //{
                //    speed = 0;
                //}
            }

            if (dragOffset > 0)
            {
                dragOffset = 0;
            }
            else if (dragOffset < maxOffset)
            {
                dragOffset = maxOffset;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Colors.Background);

            foreach (var levelButton in levelButtons)
            {
                int level = (int)levelButton.Data;
                var rawButtonRect = levelButton.Rect.ToRectangle();
                var buttonRect = new Rectangle(
                    rawButtonRect.Location + new Point(0, (int)dragOffset),
                    rawButtonRect.Size
                );

                var color = levelButton.Disabled
                    ? Colors.UnConnected
                    : Color.White;

                if (level == NextLevel)
                {
                    Game.SpriteBatch.FillRectangle(
                        buttonRect,
                        Colors.Highlighted,
                        layerDepth: Layers.BACKGROUND
                    );
                }

                Game.SpriteBatch.DrawRectangle(
                    buttonRect,
                    color,
                    thickness: levelButton.Highlighted ? 3 : 2,
                    layerDepth: Layers.BASE
                );

                Game.SpriteBatch.DrawCenteredText(
                    levelButton.Data.ToString(),
                    Game.Fonts.LevelButtonText,
                    buttonRect.Center.ToVector2(),
                    color,
                    layerDepth: Layers.BASE
                );
            }

            Game.SpriteBatch.Draw(topBar, Game.Textures, Game.Fonts);
        }
    }
}
