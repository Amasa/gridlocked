﻿// Author: Alex Leone

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gridlocked.Core.Enums;
using Gridlocked.Core.Logic;
using Gridlocked.MonoGame.Core.Helpers;
using Gridlocked.MonoGame.Core.Screens;
using Microsoft.Xna.Framework;

namespace Gridlocked.MonoGame.Core
{
    public class GridlockedGame
        : Game2D
    {
        internal override Rectangle Bounds => new Rectangle(0, 0, 300, 500);

        internal Textures Textures { get; private set; }

        internal Fonts Fonts { get; private set; }

        internal Dictionary<byte, Levels> Levels { get; } = new Dictionary<byte, Levels>();
        internal bool HasViewedTutorial { get; set; }

#if ANDROID
        const string platform = "Android";
#else
        const string platform = "Desktop";
#endif

        public GridlockedGame()
        {
            Window.Title = "Gridlocked";
        }

        protected override void Initialize()
        {
            base.Initialize();

            void UpdateGameResolution()
            {
#if ANDROID
                Graphics.PreferredBackBufferWidth = Graphics.GraphicsDevice.DisplayMode.Width;
                Graphics.PreferredBackBufferHeight = Graphics.GraphicsDevice.DisplayMode.Height;
                Graphics.IsFullScreen = true;
#elif DEBUG
                //graphics.PreferredBackBufferWidth = 575;
                //graphics.PreferredBackBufferHeight = 700;
                //graphics.PreferredBackBufferWidth = 375;
                Graphics.PreferredBackBufferWidth = 350;
                Graphics.PreferredBackBufferHeight = 625;
                Graphics.IsFullScreen = false;
#else
                Graphics.PreferredBackBufferWidth = 300;
                Graphics.PreferredBackBufferHeight = 500;
                Graphics.IsFullScreen = false;
#endif
                Resolution.SetResolution(Graphics.PreferredBackBufferWidth, Graphics.PreferredBackBufferHeight, FullScreen: Graphics.IsFullScreen);
            }

            UpdateGameResolution();
            this.Window.ClientSizeChanged += (s, e) => UpdateGameResolution();

            Levels LoadResourceText(string resourceName)
            {
                var fullResourceName = $"Gridlocked.{platform}.Content.Levels.{resourceName}";

                using var stream = this.GetType().Assembly.GetManifestResourceStream(fullResourceName);
                using var reader = new StreamReader(stream);

                string text = reader.ReadToEnd();
                
                var levels = new Levels
                {
                    EncodedLevels = text.ParseLevelList()
                };

                return levels;
            }

            // ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
            foreach (byte size in Enumerable.Range(5, 5))
            {
                Levels[size] = LoadResourceText($"{size}x{size}Levels.yaml");
            }

            // Creates and returns default save data,
            // if no save data exists yet.
            Save.Load(Levels, out var difficulty, out var sourceType, out bool hasViewedTutorial, out var gridProgress);

            // Correcting save data for a bug released in v1.2.1.
            foreach (byte size in Enumerable.Range(5, 5))
            {
                foreach (Difficulty d in Enum.GetValues(typeof(Difficulty)))
                {
                    var levelsOfSize = Levels[size];
                    if (!levelsOfSize.MaxLevelsBeaten.ContainsKey(d))
                    {
                        levelsOfSize.MaxLevelsBeaten[d] = 0;
                    }
                    if (!levelsOfSize.MaxLevelsUnlocked.ContainsKey(d))
                    {
                        levelsOfSize.MaxLevelsUnlocked[d] = 100;
                    }

                    if (levelsOfSize.MaxLevelsBeaten[d] > levelsOfSize.MaxLevelsUnlocked[d])
                    {
                        levelsOfSize.MaxLevelsBeaten[d] = levelsOfSize.MaxLevelsUnlocked[d];
                        Save.Progress(Levels, sourceType, difficulty, hasViewedTutorial, gridProgress);
                    }
                }
            }
            
            // Creates a save file on game load,
            // so it doesn't take as long to do it later.
            Save.Progress(Levels, sourceType, difficulty, hasViewedTutorial, gridProgress);

            HasViewedTutorial = hasViewedTutorial;

            if (gridProgress != null
                // The following condition is also accounting for a bug released in v1.2.1.
                && gridProgress.Level <= Levels[gridProgress.Size].MaxLevelsUnlocked[difficulty])
            { 
                LoadScreen(new GridScreen(this, sourceType, gridProgress.Size, difficulty, gridProgress.Level, gridProgress));
            }
            else
            {
                LoadScreen(new HomeScreen(this, sourceType, difficulty));
            }
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            Textures = new Textures(this.Content);
            Fonts = new Fonts(platform, this.GetType().Assembly);
        }
    }
}
