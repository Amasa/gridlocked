﻿// Author: Alex Leone

using FontStashSharp;
using System.Reflection;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal class Fonts
    {
        internal Fonts(string platform, Assembly executingAssembly)
        {
            var titleFont = new FontSystem();
            var titleFontStream = executingAssembly.GetManifestResourceStream($"Gridlocked.{platform}.Content.Fonts.SquaresBoldFree.otf");
            titleFont.AddFont(titleFontStream);

            TitleText = titleFont.GetFont(71);

            var mainFont = new FontSystem();
            var mainFontStream = executingAssembly.GetManifestResourceStream($"Gridlocked.{platform}.Content.Fonts.LiberationSans-Regular.ttf");
            mainFont.AddFont(mainFontStream);

            RegularText = mainFont.GetFont(26);
            LevelButtonText = mainFont.GetFont(22);
        }

        internal SpriteFontBase TitleText { get; private set; }
        internal SpriteFontBase RegularText { get; private set; }
        internal SpriteFontBase LevelButtonText { get; private set; }
    }
}
