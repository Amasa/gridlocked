﻿// Author: Alex Leone

using Microsoft.Xna.Framework;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal class TopBar
    {
        const int extraTopBarTopPadding = 10000;
        public TopBar(string leftText, int gameWidth)
        {
            Text = leftText;

            BackButton = new ClickableButton
            {
                Rect = new Rectangle(5 + 6, 5 + 6, 48 - 12, 48 - 12),
                Data = "Back"
            };

            Backdrop = new ClickableButton
            {
                Rect = new Rectangle(0, -extraTopBarTopPadding, gameWidth, extraTopBarTopPadding + 58),
                Data = "TopBarBackdrop"
            };
        }

        internal ClickableButton BackButton { get; private set; }

        internal ClickableButton Backdrop { get; private set; }

        internal string Text { get; private set; }
    }
}
