﻿// Author: Alex Leone

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal static class Layers
    {
        internal const float POP_UP = 0F;
        internal const float GRID = 0.1F;
        internal const float OVERLAY = 0.4F;
        internal const float BASE = 0.5F;
        internal const float BLOCK = 0.9F;
        internal const float BACKGROUND = 1F;
    }
}
