﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal static class Save
    {
#if !ANDROID
        private const string COMPANY_NAME = "Leone Gaming";
        private const string GAME_NAME = "Gridlocked";
#endif
        private const string file_name = "Gridlocked.save";

        private readonly static object saveFileLock = new object();
        private static SaveData pendingSaveData;

        internal static void Progress(Dictionary<byte, Levels> levelPacks, SourceType currentSourceType, Difficulty currentDifficulty, bool hasViewedTutorial, GridProgress gridProgress)
        {
            static void SaveToFile(SaveData saveData)
            {
                string filePath = Path.Combine(GetSaveDirectory(), file_name);

                string serializedSaveData = JsonConvert.SerializeObject(saveData);

                if (Monitor.TryEnter(saveFileLock))
                {
                    try
                    {
                        using (var stream = new StreamWriter(new FileStream(filePath, FileMode.Create)))
                        {
                            //Thread.Sleep(3000); // Useful for testing really slow filesystems, I think.
                            stream.Write(serializedSaveData);
#if DEBUG
                            System.Diagnostics.Debug.WriteLine("Game is saved.");
#endif
                        }
                    }
#if DEBUG
                    catch (Exception exception)
                    {
                        System.Diagnostics.Debug.WriteLine($"Saving threw an error: {exception.Message}");
                    }
#endif
                    finally
                    {
                        Monitor.Exit(saveFileLock);

                        if (pendingSaveData != null)
                        {
                            Task.Run(() => SaveToFile(pendingSaveData));
                            pendingSaveData = null;
                        }
                    }
                }
                else
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("Storing pending save data.");
#endif
                    pendingSaveData = saveData;
                }
            }
            
            var saveData = new SaveData
            {
                Progress = levelPacks
                    .ToDictionary(kvp => kvp.Key, kvp => new LevelPackData
                    {
                        MaxLevelsBeaten = kvp.Value.MaxLevelsBeaten,
                        MaxLevelsUnlocked = kvp.Value.MaxLevelsUnlocked
                    }),
                CurrentDifficulty = currentDifficulty,
                CurrentSourceType = currentSourceType,
                HasViewedTutorial = hasViewedTutorial,
                GridProgress = gridProgress
            };

#if DEBUG
            System.Diagnostics.Debug.WriteLine("Saving game!");
#endif

            Task.Run(() => SaveToFile(pendingSaveData ?? saveData));
        }

        /// <summary>
        /// Call AFTER loading the levels into the level packs.
        /// </summary>
        internal static void Load(Dictionary<byte, Levels> levelPacks, out Difficulty currentDifficulty, out SourceType currentSourceType, out bool hasViewedTutorial, out GridProgress gridProgress)
        {
            string filePath = Path.Combine(GetSaveDirectory(), file_name);
            
            if (File.Exists(filePath))
            {
                string serializedSaveData = File.ReadAllText(filePath);

                var saveData = JsonConvert.DeserializeObject<SaveData>(serializedSaveData);

                if (saveData != null)
                {
                    foreach (byte size in Enumerable.Range(5, 5))
                    {
                        levelPacks[size].MaxLevelsBeaten = saveData.Progress[size].MaxLevelsBeaten;
                        levelPacks[size].MaxLevelsUnlocked = saveData.Progress[size].MaxLevelsUnlocked;
                    }

                    currentDifficulty = saveData.CurrentDifficulty;
                    currentSourceType = saveData.CurrentSourceType;
                    hasViewedTutorial = saveData.HasViewedTutorial;
                    gridProgress = saveData.GridProgress;
                    
                    return;
                }
            }

            currentDifficulty = Difficulty.Normal;
            currentSourceType = SourceType.PowerStation;
            hasViewedTutorial = false;

            foreach (byte size in Enumerable.Range(5, 5))
            {
                levelPacks[size].MaxLevelsBeaten = new Dictionary<Difficulty, int>
                    {
                        { Difficulty.Normal, 0 },
                        { Difficulty.Gridlocked, 0 },
                    };
                levelPacks[size].MaxLevelsUnlocked = new Dictionary<Difficulty, int>
                    {
                        { Difficulty.Normal, 100 },
                        { Difficulty.Gridlocked, 100 },
                    };
            }

            gridProgress = null;
        }

        private class SaveData
        {
            // THESE PROPERTIES ARE PUBLIC SO THAT
            // NEWTONSOFT.JSON CAN SERIALIZE THEM.

            public Dictionary<byte, LevelPackData> Progress { get; set; }

            public Difficulty CurrentDifficulty { get; set; }
            public SourceType CurrentSourceType { get; set; }
            public bool HasViewedTutorial { get; set; }
            public GridProgress GridProgress { get; set; }
        }

        private class LevelPackData
        {
            public Dictionary<Difficulty, int> MaxLevelsBeaten { get; set; }

            public Dictionary<Difficulty, int> MaxLevelsUnlocked { get; set; }
        }

        private static string GetSaveDirectory()
        {
#if ANDROID
            string saveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
#else
            string saveDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), COMPANY_NAME, GAME_NAME);
#endif

            if (!Directory.Exists(saveDirectory))
            {
                Directory.CreateDirectory(saveDirectory);
            }

            return saveDirectory;
        }
    }
}
