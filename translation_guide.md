# Gridlocked Translation Guide

How to add new languages to Gridlocked.

## 1. Fork the repository

Create a fork of the [official repository](https://gitlab.com/Amasa/gridlocked). You'll be working with the fork.

## 2. Clone the repository on your computer

You'll be working with the files on your local computer.

## 3. Copy and rename the Strings.resx file

The `Strings.resx` file is located in `Gridlocked.Core\Resources`. It has the strings you'll be translating in it. Copy `Strings.resx` and rename the new file to `Strings.CODE.resx`, where `CODE` is the Language-Country code for the language listed in the table [here](https://gitlab.com/Amasa/gridlocked/-/blob/master/language_country_code.md). Your new `.resx` file should be in the same directory as the copied `Strings.resx` file.

For example, if you're about to translate into Norwegian, the new file name should be: `Strings.nb-NO.resx`

## 4. Open the new file in a text editor and replace the English strings with translated strings

About halfway down the `Strings.resx` file are lines like this:

```xml
<data name="Click" xml:space="preserve">
    <value>Click</value>
</data>
```

The part between the `value` tags should be translated, and everything else should remain the same.

For example, if you're translating into Norwegian, the above would become:

```xml
<data name="Click" xml:space="preserve">
    <value>Klikk</value>
</data>
```

Translate *all* the strings that look like that. Some values have new lines within the text: New lines should be preserved and *not* be indented, just like the original text.

## 5. Open the `RegularText.spritefont` file in a text editor

The SpriteFont files are located in `Gridlocked.MonoGame.Core\Content\Fonts`. Open the `RegularText.spritefont` file in a text editor.

## 6. Update the `RegularText.spritefont` file

In the `RegularText` SpriteFont file, find the lines that look like this:

```xml
<ResourceFiles>
    <Resx>../../Gridlocked.Core/Resources/Strings.resx</Resx> 
</ResourceFiles>
```

Copy the line with the `Resx` element and change the file name on the new line to point to your newly translated `.resx` files.

For example, if you just added a Norwegian translation, the above lines should look like this:

```xml
<ResourceFiles>
    <Resx>../../Gridlocked.Core/Resources/Strings.resx</Resx>
    <Resx>../../Gridlocked.Core/Resources/Strings.nb-NO.resx</Resx>
</ResourceFiles>
```

## 7. Commit your changes (leave a short, descriptive commit message) and push your commit to your fork

Now your fork has been updated with the new language, and the changes are labeled with your name (on the commit).

## 8. Submit a pull request

The pull request will be reviewed. If everything looks good, it'll be merged. Otherwise, someone may leave a comment on your pull request to make changes. Make the changes, then repeat step 7 above. Don't submit a second pull request: Your first pull request will be updated with your new commit.

When your pull request is merged, your translation will be included in Gridlocked.

---

Tada! You just contributed a new language translation to Gridlocked. :)
