﻿// Author: Alex Leone

namespace Gridlocked.Core.Enums
{
    public enum Difficulty
    {
        Normal,
        Expansion,
        Gridlocked
    }
}
