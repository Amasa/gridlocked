﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gridlocked.Core.Logic
{
    public static class Generate
    {
        private class BlockCandidates
        {
            public BlockCandidates()
            {
                foreach (var direction in Get.Enums<BlockDirection>())
                {
                    Obligations[direction] = false;
                    Neighbors[direction] = null;
                }
            }

            internal int DistanceFromStation { get; set; } = 0;

            internal bool IsHouse { get; set; } = false;

            internal Dictionary<BlockDirection, bool> Obligations { get; private set; } = new Dictionary<BlockDirection, bool>();
            
            internal Dictionary<BlockDirection, BlockCandidates> Neighbors { get; private set; } = new Dictionary<BlockDirection, BlockCandidates>();

            public override string ToString()
            {
                if (IsHouse)
                {
                    return "House";
                }

                return string.Join(", ", Obligations.Select(c => c.Value));
            }
        }

        private static bool IsHouse(this BlockCandidates block)
        {
            return block?.IsHouse == true;
        }

        private static bool IsHouseOrEdge(BlockCandidates block)
        {
            return block == null || block.IsHouse;
        }

        /// <summary>
        /// Generates a random, valid grid.
        /// ---
        /// Guidelines for generating levels:
        /// - 
        /// </summary>
        public static Grid Grid(int size, Random randomizer, out string errorMessage)
        {
            // Create and setup candidate grid.

            var blockCandidates = new BlockCandidates[size, size];

            ForEachBlock.Do(size, (x, y) =>
            {
                blockCandidates[x, y] = new BlockCandidates();
            });

            ForEachBlock.Do(size, (x, y) =>
            {
                var block = blockCandidates[x, y];

                if (y > 0)
                {
                    block.Neighbors[BlockDirection.Up] = blockCandidates[x, y - 1];
                }

                if (x < size - 1)
                {
                    block.Neighbors[BlockDirection.Right] = blockCandidates[x + 1, y];
                }

                if (y < size - 1)
                {
                    block.Neighbors[BlockDirection.Down] = blockCandidates[x, y + 1];
                }

                if (x > 0)
                {
                    block.Neighbors[BlockDirection.Left] = blockCandidates[x - 1, y];
                }
            });

            // Randomly place houses and a power station.

            int numHouses = size switch
            {
                5 => 7 + randomizer.Next(-1, 2),
                6 => 9 + randomizer.Next(-1, 2),
                7 => 12 + randomizer.Next(-2, 3),
                8 => 16 + randomizer.Next(-2, 3),
                9 => 19 + randomizer.Next(-2, 3),
                _ => throw new ArgumentOutOfRangeException()
            };

            int[] housePlusStationLocations = Enumerable.Range(0, size * size)
                .OrderBy(x => randomizer.Next())
                .Take(numHouses + 1)
                .ToArray();

            BlockCandidates powerStation = null;
            foreach (int location in housePlusStationLocations)
            {
                int x = location % size;
                int y = (int) Math.Floor((float) location / size);

                if (location == housePlusStationLocations[0])
                {
                    powerStation = blockCandidates[x, y];
                }
                else
                {
                    blockCandidates[x, y].IsHouse = true;
                }
            }

            //if (housePlusStationLocations.OrderBy(h => h).SequenceEqual(new[] { 6, 10, 11, 12, 17, 23, 24, 29, 30, 35 }))
            //{
            //    Console.WriteLine("Got it.");
            //}

            // Return an error if ANY block
            // is completely surrounded by houses, or by edges.

            foreach (var block in blockCandidates)
            {
                if (block.Neighbors.All(neighbor => IsHouseOrEdge(neighbor.Value)))
                {
                    errorMessage = "At least one block is surrounded entirely by houses or edges.";
                    return null;
                }
            }

            // Start pathfinding. Fill in all the non-house blocks.

            static bool IsValidPathfindingTarget(BlockCandidates block)
            {
                return !IsHouseOrEdge(block) &&
                    block.Obligations.All(candidate => candidate.Value == false);
            }

            IReadOnlyCollection<BlockDirection> allDirections = Get.Enums<BlockDirection>();

            var pathEnd = powerStation;

            var path = new HashSet<BlockCandidates>();

            while (path.Count < blockCandidates.Length)
            {
                path.Add(pathEnd);

                var neighbors = pathEnd.Neighbors
                    .Where(neighbor => IsValidPathfindingTarget(neighbor.Value))
                    .ToArray();

                if (neighbors.Length == 0)
                {
                    // Find a branching point along the path, set that as the new pathEnd.

                    var potentialBranchingPoints = path
                        .Where(block =>
                            block.Obligations.Count(candidate => candidate.Value == true) < 3 &&
                            block != pathEnd &&
                            allDirections.Any(direction =>
                                IsValidPathfindingTarget(block.Neighbors[direction]) &&
                                block.Obligations[direction] == false
                            )
                        )
                        .ToArray();

                    if (potentialBranchingPoints.Length == 0)
                    {
                        // If there are NO valid branching paths, then we're done pathfinding.

                        break;
                    }

                    var branchingPoint = Get.RandomItemFrom(potentialBranchingPoints, randomizer);

                    pathEnd = branchingPoint;

                    continue;
                }

                // Pick a random direction, then
                // connect this block to the block in that direction,
                // and connect that block back to this one.

                var nextNeighbor = Get.RandomItemFrom(neighbors, randomizer);
                pathEnd.Obligations[nextNeighbor.Key] = true;
                nextNeighbor.Value.Obligations[nextNeighbor.Key.Rotate(2)] = true;
                

                // Start again, but with the new block.

                nextNeighbor.Value.DistanceFromStation = pathEnd.DistanceFromStation + 1;
                pathEnd = nextNeighbor.Value;
            }

            if (path.Count >= blockCandidates.Length)
            {
                throw new InvalidOperationException("Error generating grid: Pathfinding algorithm appears to be broken.");
            }

            var blockCandidatesList = new List<BlockCandidates>();
            foreach (var block in blockCandidates)
            {
                blockCandidatesList.Add(block);
            }

            if (blockCandidatesList.Any(block => !block.IsHouse() && block.Obligations.All(candidate => candidate.Value == false)))
            {
                errorMessage = "Pathfinding was cut off by houses.";
                return null;
            }

            // Connect dead ends to houses.
            // First, connect dead ends with only one neighboring house.
            // Then, connect houses that are only adjacent to that dead end.
            // Then, connect all other dead ends to random houses.
            // If any dead end can't be connected to a house, return an error.

            var deadEnds = blockCandidatesList
                .Where(block =>
                    !block.IsHouse &&
                    block != powerStation &&
                    block.Obligations.Count(c => c.Value == true) == 1
                )
                .OrderBy(deadEnd => deadEnd.Neighbors.Count(n => n.Value.IsHouse()) == 1 ? 0 : 1)
                .ThenBy(deadEnd => deadEnd.Neighbors.Any(n =>
                    n.Value != null &&
                    n.Value.IsHouse() &&
                    n.Value.Neighbors.All(hn => IsHouseOrEdge(hn.Value) || hn.Value.Obligations.Count(c => c.Value == true) == 3 || hn.Value == deadEnd)
                ))
                .ThenBy(deadEnd => randomizer.Next())
                .ToArray();

            foreach (var deadEnd in deadEnds)
            {
                var potentialHouses = deadEnd.Neighbors
                    .Where(neighbor =>
                        neighbor.Value != null &&
                        neighbor.Value.IsHouse() &&
                        neighbor.Value.Obligations.All(c => c.Value == false)
                    )
                    .GroupBy(neighbor =>
                        neighbor.Value.Neighbors.Count(n =>
                            n.Value != null &&
                            n.Value.Obligations.Count(c => c.Value == true) == 1 &&
                            !n.Value.IsHouse
                        )
                    )
                    .OrderBy(group => group.Key)
                    .Take(1)
                    .SelectMany(g => g)
                    .Select(g => g.Value)
                    .ToArray();

                if (potentialHouses.Length == 0)
                {
                    errorMessage = "At least one dead end has no potential houses to connect to.";
                    return null;
                }

                var houseToConnect = Get.RandomItemFrom(potentialHouses, randomizer);

                var directionToHouse = deadEnd.Neighbors
                    .Where(neighbor => neighbor.Value == houseToConnect)
                    .Select(neighbor => neighbor.Key)
                    .Single();

                deadEnd.Obligations[directionToHouse] = true;
                houseToConnect.Obligations[directionToHouse.Rotate(2)] = true;
            }

            // Connect other houses to wires.

            var unconnectedHouses = blockCandidatesList
                .Where(block => block.IsHouse() && block.Obligations.All(candidate => candidate.Value == false))
                .OrderBy(house => house.Neighbors.Values
                    .Count(neighbor => neighbor != null && neighbor.Obligations.Count(c => c.Value == true) < 3)
                )
                .ToArray();

            foreach (var house in unconnectedHouses)
            {
                var connectableBlocks = house.Neighbors
                    .Where(neighbor =>
                        neighbor.Value != null &&
                        !neighbor.Value.IsHouse() &&
                        neighbor.Value.Obligations.Count(c => c.Value == true) < 3)
                    .ToArray();

                if (connectableBlocks.Length == 0)
                {
                    errorMessage = "At least one house has no valid blocks to connect itself to.";
                    return null;
                }

                // Prioritize blocks that are further from the power station.
                var blockToConnectTo = connectableBlocks
                    .OrderByDescending(block => block.Value.DistanceFromStation)
                    .ThenBy(block => randomizer.Next())
                    .First();

                var directionToBlock = blockToConnectTo.Key;

                house.Obligations[directionToBlock] = true;
                blockToConnectTo.Value.Obligations[directionToBlock.Rotate(2)] = true;
            }


            // Convert block candidates array to usable block array.

            var blocks = new Block[size, size];

            ForEachBlock.Do(size, (x, y) =>
            {
                var block = blockCandidates[x, y];

                bool isPowerStation = block == powerStation;
                bool isHouse = block.IsHouse;

                var type = BlockType.Stub;
                var direction = block.Obligations.First(obligation => obligation.Value == true).Key;

                if (!isHouse)
                {
                    int numDirections = block.Obligations.Count(obligation => obligation.Value == true);
                    if (numDirections > 3)
                    {
                        throw new InvalidOperationException($"Error: Block {x},{y} has more than 3 valid candidate directions, which is not allowed.");
                    }
                    if (numDirections == 3)
                    {
                        type = BlockType.TCross;
                        direction = block.Obligations.First(obligation => obligation.Value == false).Key.Rotate(2);
                    }
                    else if (numDirections == 2)
                    {
                        if (block.Obligations.Any(obligation =>
                                obligation.Value == true &&
                                block.Obligations[obligation.Key.Rotate(2)] == true
                            )
                        )
                        {
                            type = BlockType.StraightLine;
                            direction = block.Obligations.First(obligation => obligation.Value == true).Key;
                        }
                        else
                        {
                            type = BlockType.LBend;
                            direction = block.Obligations
                                .First(obligation =>
                                    obligation.Value == true &&
                                    block.Obligations[obligation.Key.Rotate(1)] == true
                                ).Key;
                        }
                    }
                }

                blocks[x, y] = new Block(type, isPowerStation, isHouse)
                {
                    Direction = direction
                };
            });

            // CONNECT ALL THE BLOCKS
            var grid = new Grid
            {
                Blocks = blocks,
                IsSolved = false
            };

            ForEachBlock.Do(size, (x, y) =>
            {
                var block = blocks[x, y];

                block.ConnectToAdjacentBlocks(grid, x, y);
            });

            grid.UpdateSolvedState();

            if (!grid.IsSolved)
            {
                throw new Exception("Error: Level was not able to be created solved, which is not allowed.");
            }

            // Throw out levels that have unnecessary blocks that we can identify.

            foreach (var startingTCross in blocks)
            {
                if (startingTCross.Type != BlockType.TCross)
                {
                    continue;
                }

                var checkDirection = startingTCross.Direction.Rotate(2);

                var partnerBlock = startingTCross.BlockTo(checkDirection);

                if (partnerBlock == null ||
                    partnerBlock.Type == BlockType.StraightLine)
                {
                    continue;
                }

                // Try to connect to the tCross from the partnerBlock.

                var startingDirections = partnerBlock.ConnectedDirections();

                foreach (var startingDirection in startingDirections)
                {
                    var currentBlock = partnerBlock.BlockTo(startingDirection);
                    var lastDirection = startingDirection;
                    int failsafe = 0;

                    while (!currentBlock.IsPowerStation && failsafe < blocks.Length)
                    {
                        if (currentBlock.Type == BlockType.StraightLine)
                        {
                            // Change nothing about the direction.
                            // Left in for readability--nothing happens here.

                            //lastDirection = lastDirection;
                        }
                        else if (currentBlock.Type == BlockType.LBend)
                        {
                            var possibleDirections = currentBlock.ConnectedDirections();
                            lastDirection = possibleDirections.Single(direction => direction != lastDirection.Rotate(2));
                        }
                        else
                        {
                            break;
                        }

                        currentBlock = currentBlock.BlockTo(lastDirection);

                        failsafe += 1;
                    }

                    if (failsafe >= blocks.Length)
                    {
                        throw new Exception("Error: Loop-finding algorithm appears to be broken.");
                    }

                    if (currentBlock == startingTCross)
                    {
                        errorMessage = "An unnecessary loop was generated on accident.";
                        return null;
                    }
                }
            }


            errorMessage = null;
            return grid;
        }
    }
}
