﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Models;
using System;

namespace Gridlocked.Core.Logic
{
    public static class UpdateGrid
    {
        public static BlockDirection Rotate(this BlockDirection direction, int times = 1)
        {
            var directionNum = (int)direction;

            directionNum += times % 4;

            if (directionNum >= 4)
            {
                directionNum -= 4;
            }
            else if (directionNum < 0)
            {
                directionNum += 4;
            }
            
            return (BlockDirection)directionNum;
        }

        public static void Reset(this Grid grid, Random randomizer)
        {
            foreach (var block in grid.Blocks)
            {
                block.Direction = Get.RandomItemFrom<BlockDirection>(randomizer);
                block.IsLocked = false;
            }

            grid.LastBlockRotated = null;

            grid.UpdateSolvedState();
        }

        public static void Set(this Grid grid, GridProgress gridProgress)
        {
            if (gridProgress == null)
            {
                return;
            }

            grid.LastBlockRotated = null;

            int size = grid.Blocks.GetLength(0);

            int x = 0, y;
            while (x < size)
            {
                y = 0;

                while (y < size)
                {
                    var block = grid.Blocks[x, y];
                    block.Direction = gridProgress.Directions[x, y];
                    
                    var isLocked = gridProgress.IsLockeds[x, y];
                    if (isLocked == null)
                    {
                        grid.LastBlockRotated = block;
                    }
                    else
                    {
                        block.IsLocked = isLocked.Value;
                    }

                    y += 1;
                }

                x += 1;
            }


            grid.UpdateSolvedState();
        }

        public static GridProgress GetProgress(this Grid grid, int level)
        {
            byte size = (byte) grid.Blocks.GetLength(0);
            var directions = new BlockDirection[size, size];
            var isLockeds = new bool?[size, size];

            int x = 0, y;
            while (x < size)
            {
                y = 0;

                while (y < size)
                {
                    var block = grid.Blocks[x, y];
                    directions[x, y] = block.Direction;

                    if (grid.Blocks[x, y] == grid.LastBlockRotated)
                    {
                        isLockeds[x, y] = null;
                    }
                    else
                    {
                        isLockeds[x, y] = block.IsLocked;
                    }

                    y += 1;
                }

                x += 1;
            }

            var gridProgress = new GridProgress
            {
                Directions = directions,
                IsLockeds = isLockeds,
                Size = size,
                Level = level
            };

            return gridProgress;
        }

        public static void Deselect(this Grid grid)
        {
            if (grid.Difficulty == Difficulty.Gridlocked
                && grid.LastBlockRotated != null)
            {
                grid.LastBlockRotated.IsLocked = true;
            }

            grid.LastBlockRotated = null;
        }

        public static void RotateBlock(this Grid grid, byte x, byte y)
        {
            // Locked blocks can't be rotated.
            var blockToRotate = grid.Blocks[x, y];
            if (blockToRotate.IsLocked)
            {
                return;
            }

            // ROTATE BLOCK
            blockToRotate.Direction = blockToRotate.Direction.Rotate();

            // GRIDLOCKED MODE
            if (grid.Difficulty == Difficulty.Gridlocked
                && grid.LastBlockRotated != null
                && grid.LastBlockRotated != blockToRotate)
            {
                // Lock the last block rotated.
                grid.LastBlockRotated.IsLocked = true;
            }

            grid.LastBlockRotated = blockToRotate;

            grid.UpdateSolvedState();

            if (grid.IsSolved)
            {
                grid.LastBlockRotated = null;
            }
        }

        internal static void UpdatePoweredState(this Grid grid)
        {
            Block powerStation = null;
            int size = grid.Blocks.GetLength(0);

            ForEachBlock.Do(size, (x, y) =>
            {
                var block = grid.Blocks[x, y];
                if (block.IsPowerStation)
                {
                    powerStation = block;
                }
                else
                {
                    block.IsPowered = false;
                }
            });

            if (powerStation == null)
            {
                throw new InvalidOperationException("Error updating grid: Game couldn't find a power station!");
            }

            static void PowerBlock(Block block, bool source = false)
            {
                if (block == null)
                {
                    return;
                }

                if (block.IsPowered && !source)
                {
                    return;
                }
                
                block.IsPowered = true;

                if (block.ConnectedUp() && block.BlockAbove.ConnectedDown())
                {
                    PowerBlock(block.BlockAbove);
                }

                if (block.ConnectedRight() && block.BlockToRight.ConnectedLeft())
                {
                    PowerBlock(block.BlockToRight);
                }

                if (block.ConnectedLeft() && block.BlockToLeft.ConnectedRight())
                {
                    PowerBlock(block.BlockToLeft);
                }

                if (block.ConnectedDown() && block.BlockBelow.ConnectedUp())
                {
                    PowerBlock(block.BlockBelow);
                }
            }

            PowerBlock(powerStation, source: true);

            return;
        }

        internal static void UpdateSolvedState(this Grid grid)
        {
            grid.UpdatePoweredState();

            bool isSolved = true;
            
            foreach (var block in grid.Blocks)
            {
                if (!block.IsPowered || !block.TotallyConnected())
                {
                    isSolved = false;
                    break;
                }
            }

            grid.IsSolved = isSolved;

            // EXPANSION MODE
            if (grid.Difficulty == Difficulty.Expansion)
            {
                foreach (var block in grid.Blocks)
                {
                    bool couldBePoweredByRotating = block.IsPowered
                        || (block.BlockAbove != null && block.BlockAbove.IsPowered && block.BlockAbove.ConnectedDown())
                        || (block.BlockBelow != null && block.BlockBelow.IsPowered && block.BlockBelow.ConnectedUp())
                        || (block.BlockToLeft != null && block.BlockToLeft.IsPowered && block.BlockToLeft.ConnectedRight())
                        || (block.BlockToRight != null && block.BlockToRight.IsPowered && block.BlockToRight.ConnectedLeft());

                    block.IsLocked = !couldBePoweredByRotating;
                }
            }
        }
    }
}
