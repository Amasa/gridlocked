﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Models;
using System;
using System.Linq;

namespace Gridlocked.Core.Logic
{
    internal static class BlockConnected
    {
        internal static void ConnectToAdjacentBlocks(this Block block, Grid grid, byte x, byte y)
        {
            var size = grid.Blocks.GetLength(0);

            if (y > 0)
            {
                block.BlockAbove = grid.Blocks[x, y - 1];
            }

            if (x < size - 1)
            {
                block.BlockToRight = grid.Blocks[x + 1, y];
            }

            if (y < size - 1)
            {
                block.BlockBelow = grid.Blocks[x, y + 1];
            }

            if (x > 0)
            {
                block.BlockToLeft = grid.Blocks[x - 1, y];
            }
        }

        internal static bool TotallyConnected(this Block block)
        {
            var allDirections = Get.Enums<BlockDirection>();

            foreach (var direction in allDirections)
            {
                if (block.Connected(direction) && !block.BlockTo(direction).Connected(direction.Rotate(2)))
                {
                    return false;
                }
            }

            return true;
        }

        internal static bool Connected(this Block block, BlockDirection direction)
        {
            var directionsConnected = block.ConnectedDirections();

            return directionsConnected.Contains(direction);
        }

        internal static bool ConnectedUp(this Block block)
        {
            return block.Connected(BlockDirection.Up);
        }

        internal static bool ConnectedRight(this Block block)
        {
            return block.Connected(BlockDirection.Right);
        }

        internal static bool ConnectedLeft(this Block block)
        {
            return block.Connected(BlockDirection.Left);
        }

        internal static bool ConnectedDown(this Block block)
        {
            return block.Connected(BlockDirection.Down);
        }

        internal static BlockDirection[] ConnectedDirections(this Block block)
        {
            if (block == null)
            {
                return Array.Empty<BlockDirection>();
            }

            var type = block.Type;
            var direction = block.Direction;

            return type switch
            {
                BlockType.Stub => direction switch
                {
                    BlockDirection.Up => new[] { BlockDirection.Up },
                    BlockDirection.Right => new[] { BlockDirection.Right },
                    BlockDirection.Down => new[] { BlockDirection.Down },
                    BlockDirection.Left => new[] { BlockDirection.Left },
                    _ => throw new ArgumentOutOfRangeException()
                },
                BlockType.StraightLine => direction switch
                {
                    var d when d == BlockDirection.Up || d == BlockDirection.Down => new[] { BlockDirection.Up, BlockDirection.Down },
                    var d when d == BlockDirection.Left || d == BlockDirection.Right => new[] { BlockDirection.Left, BlockDirection.Right },
                    _ => throw new ArgumentOutOfRangeException()
                },
                BlockType.LBend => direction switch
                // LBend continues clockwise from its direction.
                {
                    BlockDirection.Up => new[] { BlockDirection.Up, BlockDirection.Right },
                    BlockDirection.Right => new[] { BlockDirection.Right, BlockDirection.Down },
                    BlockDirection.Down => new[] { BlockDirection.Down, BlockDirection.Left },
                    BlockDirection.Left => new[] { BlockDirection.Left, BlockDirection.Up },
                    _ => throw new ArgumentOutOfRangeException()
                },
                BlockType.TCross => direction switch
                {
                    BlockDirection.Up => new[] { BlockDirection.Left, BlockDirection.Up, BlockDirection.Right },
                    BlockDirection.Right => new[] { BlockDirection.Up, BlockDirection.Right, BlockDirection.Down },
                    BlockDirection.Down => new[] { BlockDirection.Right, BlockDirection.Down, BlockDirection.Left },
                    BlockDirection.Left => new[] { BlockDirection.Down, BlockDirection.Left, BlockDirection.Up },
                    _ => throw new ArgumentOutOfRangeException()
                },
                _ => throw new ArgumentOutOfRangeException(),
            };
        }
    }
}
