﻿// Author: Alex Leone

using Gridlocked.Core.Enums;

namespace Gridlocked.Core.Models
{
    public class GridProgress
    {
        public BlockDirection[,] Directions { get; set; }
        public bool?[,] IsLockeds { get; set; }
        public byte Size { get; set; }
        public int Level { get; set; }
    }
}
