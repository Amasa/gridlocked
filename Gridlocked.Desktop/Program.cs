﻿using Gridlocked.MonoGame.Core;
using System;

namespace Gridlocked.Desktop
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using var game = new GridlockedGame();

#if DEBUG
            //game.Window.IsBorderless = true;

            var foreignLanguageTestCulture = new System.Globalization.CultureInfo("de-DE");
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = foreignLanguageTestCulture;
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = foreignLanguageTestCulture;
#endif

            game.Run();
        }
    }
}
