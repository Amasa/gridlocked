﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Logic;
using NUnit.Framework;

namespace Gridlocked.Test.Logic
{
    public class UpdateGridTests
    {
        [TestCase(BlockDirection.Up, 1, BlockDirection.Right)]
        [TestCase(BlockDirection.Up, 2, BlockDirection.Down)]
        [TestCase(BlockDirection.Up, 3, BlockDirection.Left)]
        [TestCase(BlockDirection.Up, 4, BlockDirection.Up)]
        [TestCase(BlockDirection.Right, 1, BlockDirection.Down)]
        [TestCase(BlockDirection.Right, 2, BlockDirection.Left)]
        [TestCase(BlockDirection.Right, 3, BlockDirection.Up)]
        [TestCase(BlockDirection.Right, 4, BlockDirection.Right)]
        [TestCase(BlockDirection.Down, 1, BlockDirection.Left)]
        [TestCase(BlockDirection.Down, 2, BlockDirection.Up)]
        [TestCase(BlockDirection.Down, 3, BlockDirection.Right)]
        [TestCase(BlockDirection.Down, 4, BlockDirection.Down)]
        [TestCase(BlockDirection.Left, 1, BlockDirection.Up)]
        [TestCase(BlockDirection.Left, 2, BlockDirection.Right)]
        [TestCase(BlockDirection.Left, 3, BlockDirection.Down)]
        [TestCase(BlockDirection.Left, 4, BlockDirection.Left)]
        public void UpdateGrid_Rotate_RotatesToCorrectDirection(
            BlockDirection initialDirection,
            byte times,
            BlockDirection expectedDirection)
        {

            var actualDirection = initialDirection.Rotate(times);

            Assert.AreEqual(expectedDirection, actualDirection);
        }
    }
}
