// Author: Alex Leone

using NUnit.Framework;
using Gridlocked.Core.Logic;
using System.Reflection;
using System.IO;

namespace Gridlocked.Test
{
    public class SolveTests
    {
        // TODO: Somewhere in this module, create tests to make sure that all levels are UNIQUE.

        // TODO: Also, make sure that all levels are SOLVABLE.

        static string[] SolvableLevels()
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream("Gridlocked.Test.Logic.solvable_levels.yaml"))
            using (var reader = new StreamReader(stream))
            {
                string fileText = reader.ReadToEnd();

                var solvableLevels = fileText.ParseLevelList();
                return solvableLevels;
            }

        }

        [TestCaseSource(nameof(SolvableLevels))]
        public void KnownGoodLevel_IsSolvable(string levelCode)
        {
            var grid = levelCode.Decode();

            bool isSolvable = grid.IsSolvable(out var ignore);

            Assert.IsTrue(isSolvable);
        }
    }
}